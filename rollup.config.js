import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import { terser } from 'rollup-plugin-terser';
import builtins from 'builtin-modules';
import run from '@rollup/plugin-run';

const dev = process.env.ROLLUP_WATCH;
const production = !dev;

export default {
  input: './app.js',
  output: {
    file: './dist/bundle.js',
    format: 'cjs',
    sourcemap: true,
  },
  plugins: [
    resolve({
      preferBuiltins: true,
    }),
    commonjs(),
    dev && run(),
    production && terser(),
  ],
  external: builtins,
};
