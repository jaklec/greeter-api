# syntax=docker/dockerfile:1.0.0-experimental
FROM node:13-alpine AS build


RUN apk add git openssh-client
RUN mkdir -p -m 0600 /root/.ssh && ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts

ARG SSH_PRIVATE_KEY
RUN echo "${SSH_PRIVATE_KEY}" > /root/.ssh/id_rsa
RUN chmod 600 /root/.ssh/id_rsa

WORKDIR /app
COPY *.yml *.json *.js ./
# RUN --mount=type=ssh npm install
# RUN --mount=type=ssh,id=bitbucket npm install 
# RUN ssh-agent sh -c 'echo $SSH_KEY | base64 -d | ssh-add - ; npm install'
RUN npm install
RUN npm run build

FROM node:13-alpine
WORKDIR /app
COPY --from=build /app/dist/bundle.js ./

CMD ["node", "bundle.js"]
