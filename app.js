const polka = require('polka');
const lib = require('greetings-lib');

const port = 3000;

polka()
  .get('/', (req, res) => {
    res.end(`OK`);
  })
  .get('/hello/:name', (req, res) => {
    const { name } = req.params;
    if (name) {
      res.end(lib.greeter.greeting(name));
    } else {
      res.status(404).end();
    }
  })
  .listen(port, err => {
    if (err) throw err;
    console.log(`> Running on localhost:${port}`);
  });
